# GetSpectrum.jl

This simple package computes the power spcetral density of an input signal `x[n]` sampled at the samplinfg frequency `fs`./
If the function is called inly with an input signal without the sampling frequency, the Nyquist frequency (`fs=1`) is used instead
The keyword N can be used to reduce the signal size to N. 


            using Plots 
            # Parameters
            N  = 2048;
            Fs = 42e3;
            # Generate a tone @300Hz
            x = exp(2im*pi*300/Fs .*(0:N-1));
            # Compute spectrum
            (xAx,psd) = getSpectrum(Fs,x);
            # 
            plot(xAx,psd);
            